xquery version "3.0";

import module namespace login="http://exist-db.org/xquery/login" at "resource:org/exist/xquery/modules/persistentlogin/login.xql";
import module namespace config="http://www.tei-c.org/tei-simple/config" at "modules/config.xqm";

declare variable $exist:path external;
declare variable $exist:resource external;
declare variable $exist:controller external;
declare variable $exist:prefix external;
declare variable $exist:root external;

declare variable $logout := request:get-parameter("logout", ());
declare variable $login := request:get-parameter("user", ());

declare variable $local:HTTP_OK := xs:integer(200);
declare variable $local:HTTP_CREATED := xs:integer(201);
declare variable $local:HTTP_NO_CONTENT := xs:integer(204);
declare variable $local:HTTP_BAD_REQUEST := xs:integer(400);
declare variable $local:HTTP_UNAUTHORIZED := xs:integer(401);
declare variable $local:HTTP_FORBIDDEN := xs:integer(403);
declare variable $local:HTTP_NOT_FOUND := xs:integer(404);
declare variable $local:HTTP_METHOD_NOT_ALLOWED := xs:integer(405);
declare variable $local:HTTP_INTERNAL_SERVER_ERROR := xs:integer(500);

if ($exist:path eq '') then
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <redirect url="{request:get-uri()}/"/>
    </dispatch>

else if ($exist:path eq "/") then
(: forward root path to index.xql :)
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <redirect url="home.html"/>
    </dispatch>

else if (contains($exist:path, "/$shared/")) then
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <forward url="/shared-resources/{substring-after($exist:path, '/$shared/')}"/>
    </dispatch>

else if (contains($exist:path, "/resources")) then
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <forward url="{$exist:controller}/resources/{substring-after($exist:path, '/resources/')}"/>
    </dispatch>

else if (contains($exist:path, "/components")) then
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <forward url="{$exist:controller}/components/{substring-after($exist:path, '/components/')}"/>
    </dispatch>

else if (contains($exist:path, "/bower_components")) then
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <forward url="{$exist:controller}/components/bower_components/{substring-after($exist:path, '/bower_components/')}"/>
    </dispatch>

else if (contains($exist:path, "/node_modules")) then
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <forward url="{$exist:controller}/node_modules/{substring-after($exist:path, '/node_modules/')}"/>
    </dispatch>

else if (contains($exist:path, "/elements")) then
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <forward url="{$exist:controller}/elements/{substring-after($exist:path, '/elements/')}"/>
        </dispatch>
else if (ends-with($exist:resource, ".xql")) then (
        login:set-user($config:login-domain, (), false()),
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <forward url="{$exist:controller}/modules/{$exist:resource}"/>
            <cache-control cache="no"/>
        </dispatch>
)
else if ($logout or $login) then (
    login:set-user($config:login-domain, (), false()),
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <redirect url="{replace(request:get-uri(), "^(.*)\?", "$1")}"/>
    </dispatch>
)
else if ($exist:resource = "login") then (
    util:declare-option("exist:serialize", "method=json media-type=application/json"),
    try {
        login:set-user($config:login-domain, (), false()),
        if ((sm:id()//sm:username/text()) != "guest") then (
            response:set-status-code($local:HTTP_OK),
            <response>
                <user>{sm:id()//sm:username/text()}</user>
                <isDba>{sm:is-dba(sm:id()//sm:username/text())}</isDba>
            </response>
        )
        else (
            response:set-status-code($local:HTTP_OK),
            <response>
                <fail>Authentication failed -- please check your credentials and try again.</fail>
                <currentuser>{sm:id()//sm:username/text()}</currentuser>
            </response>
        )
    } catch * {
        response:set-status-code($local:HTTP_INTERNAL_SERVER_ERROR),
        <response>
            <fail>{$err:description}</fail>
        </response>
    }
)
else if (ends-with($exist:resource, ".html")) then (
    login:set-user($config:login-domain, (), false()),
    (: the html page is run through view.xql to expand templates :)
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <forward url="{$exist:controller}/templates/{$exist:resource}"/> :)
        <view>
            <forward url="{$exist:controller}/templates/{$exist:resource}" method="get"/>
            <forward url="{$exist:controller}/modules/view.xql"/>
        </view>
        <error-handler>
            <forward url="{$exist:controller}/templates/error-page.html" method="get"/>
            <forward url="{$exist:controller}/modules/view.xql"/>
        </error-handler>
    </dispatch>
) else if (starts-with($exist:path, "/works/") or starts-with($exist:path, "/review/")) then (
    login:set-user($config:login-domain, (), false()),
    let $id := replace(xmldb:decode($exist:resource), "^(.*)\..*$", "$1")
    let $html :=
        if ($exist:resource = "") then
            if (starts-with($exist:path, "/works/")) then
                "templates/browse.html"
            else
                "templates/review.html"
        else if ($exist:resource = ("search.html", "toc.html")) then
            $exist:resource
        else
            "templates/view.html"
    return
        if (ends-with($exist:resource, ".epub")) then
            <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                <forward url="{$exist:controller}/modules/get-epub.xql">
                    <add-parameter name="id" value="{$id}"/>
                </forward>
                <error-handler>
                    <forward url="{$exist:controller}/templates/error-page.html" method="get"/>
                    <forward url="{$exist:controller}/modules/view.xql"/>
                </error-handler>
            </dispatch>
         else if (ends-with($exist:resource, ".tei")) then
            <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                <forward url="{$exist:controller}/modules/get-tei.xql">
                    <add-parameter name="id" value="{$id}"/>
                </forward>
                <error-handler>
                    <forward url="{$exist:controller}/templates/error-page.html" method="get"/>
                    <forward url="{$exist:controller}/modules/view.xql"/>
                </error-handler>
            </dispatch>
        else if (ends-with($exist:resource, ".tex")) then
                <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                    <forward url="{$exist:controller}/modules/latex.xql">
                        <add-parameter name="id" value="{$id}"/>
                    </forward>
                    <error-handler>
                        <forward url="{$exist:controller}/templates/error-page.html" method="get"/>
                        <forward url="{$exist:controller}/modules/view.xql"/>
                    </error-handler>
                </dispatch>
        else if (ends-with($exist:resource, ".pdf")) then
                <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                    <forward url="{$exist:controller}/modules/pdf.xql">
                        <add-parameter name="doc" value="{$id}.xml"/>
                    </forward>
                    <error-handler>
                        <forward url="{$exist:controller}/templates/error-page.html" method="get"/>
                        <forward url="{$exist:controller}/modules/view.xql"/>
                    </error-handler>
                </dispatch>
        else if (ends-with($exist:resource, ".plain")) then
            <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                <forward url="{$exist:controller}/modules/transform.xql">
                    <set-header name="Cache-Control" value="no-cache"/>
                        <add-parameter name="doc" value="{$id}.xml"/>
                </forward>
            </dispatch>
        else
            <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                <forward url="{$exist:controller}/{$html}"></forward>
                <view>
                    <forward url="{$exist:controller}/modules/view.xql">
                        {
                            if ($exist:resource != "toc.html") then
                                <add-parameter name="doc" value="{$id}.xml"/>
                            else
                                ()
                        }
                        <set-header name="Cache-Control" value="no-cache"/>
                    </forward>
                </view>
                <error-handler>
                    <forward url="{$exist:controller}/templates/error-page.html" method="get"/>
                    <forward url="{$exist:controller}/modules/view.xql"/>
                </error-handler>
            </dispatch>
) else
(: everything else is passed through :)
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <cache-control cache="yes"/>
    </dispatch>
