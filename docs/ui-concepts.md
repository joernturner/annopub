#  UI concepts

I will stick to Martin's division of areas into top banner, navigation (TOC) panel,
text display panel in the centre and annotation panel to the right.

Furthermore my comments concern the 'Read' view and the use case where annotations
are made while reading through the text, which is what we discussed today.
You will find wireframes for particular situations attached.

## 1. Accessing summary of attributes and existing annotations

![context information](hover.png)

What user sees in the text display panel is usually a page of text
(see hover.png wireframe). Defective words, which were already marked up as
such are highlighted in red. Words already annotated will be highlighted in
yellow while annotation is awaiting review[1]. Hovering over a word with a
mouse brings up a tooltip box displaying summarized information on that
particular word. That will contain word's xml:id and all other attributes
and their values (like lemma, pos or reg). Below attributes user would find
a list of all annotations targeting that word. This tooltip will be read only
and as it's connected with the word user is hovering over, it will change as
user moves the mouse from one word to another.

[1] after annotation has been approved, the resulting changes are seamlessly merged into document's TEI source and no longer highlighted in the reading view, as we consider this an established encoding requiring no further attention. Record of that activity remains obviously available in the annotation list stored in the database, but interface for retrieving and displaying it is beyond scope for next weeks.

2. Creating editorial annotations - edit.png

Next step happens when the user finds a word which she wants to edit or comment on. Let's consider the former case first. Upon clicking on a word, the annotation panel on the right hand side is filled with an edit form as shown in edit.png wireframe.

a. single word selection - update
most typical case is when a single word was selected. Edit form in the annotation panel shows then the existing text content and all attribute values together with input fields allowing to change any of those. The list of attributes can be adjusted and wireframe shows just a few to illustrate the concept. In addition to word's content and attributes user can add a comment that relates to the edits she's made (so changes to the text content or attribute values). NB: it is not meant for exegetical annotations.

b. multiple word selection - join (join.png)
in case of multiple word selection it is assumed that the user aims to join words, which can be achieved by removing whitespace from the text content input field. Eventually (after approval of such an annotation) multiple <w> elements will be substituted with one, retaining the xml:id of the first selected token and attribute values from the edit form.

c. word split
splitting word happens in a similar manner - adding a whitespace into the text content creates a 'join' type annotation, which after approval results in a new <w> element being added to the TEI source document. NB: xml:ids for such newly created elements are right now arbitrary and quite ugly. We intend to prettify this at an export stage where it's easier to assure consistent numbering.

3. Exegetical annotations - comment.png
For adding longer annotations commenting on the selected text, user will need to switch to the 'Comment' tab in the annotation panel. A larger input box will appear together with an option for marking the annotation as 'private', so visible only for its author.

4. Layout - toc.png
Due to constraints in horizontal space available, we've decided to hide the navigation (TOC) panel by default.

5. Color-coding
As already noted, defective words and annotation status are characteristics of different objects - first one of <w> elements and the second of annotations.
Thus only defects will be represented as @type attribute on <w> and the latter stored in the annotation record.

We have discussed two scenarios
a. user view
- defective words highlighted in red (perhaps with a red wavy underline as typical of text editors)
- words with pending annotations highlighted in yellow
As single word can be defective and have one or more annotations pending at the same time, it is necessary to differentiate the manner of highlighting (eg using border or underline and background color).

b. reviewer mode
Defective words are not really relevant here, while reviewer's attention should be brought to annotations awaiting review. Thus annotated words will be highlighted in yellow (following traffic lights colors). As reviewer accepts or rejects it, annotation highlight will change into green or red.

c. selection highlight
current selection is represented with a blue highlight, which will turn into yellow (pending annotation) or possibly also red (defective word) following creation of an annotation ticket.

6. Review process approve.png
Clicking on an annotated word brings up the edit form in the annotation panel. Reviewer is presented with Accept and Reject buttons, but in addition to that she might choose to fill the 'reviewer note' field.

7. Annotation model

Edit form in the annotation panel is a certain aggregation, allowing to edit distinct characteristics of a token on a single screen which Martin called an 'annotation ticket'. As every input field of the ticket represents an atomic action (eg regularization) it is stored internally as separate stand-off annotation.

Similarly rejecting or approving the ticket means in fact rejecting or approving all atomic annotations it consists of. Reviewer may also change some of the values proposed by the user. Such an act should be interpreted (and will be handled) as rejecting user's atomic annotation for that field and creating a new one (with reviewer as an author) with updated value.I will stick to Martin's division of areas into top banner, navigation (TOC) panel, text display panel in the centre and annotation panel to the right.

Furthermore my comments concern the 'Read' view and the use case where annotations are made while reading through the text, which is what we discussed today. You will find wireframes for particular situations attached.

1. Accessing summary of attributes and existing annotations - hover.png

What user sees in the text display panel is usually a page of text (see hover.png wireframe). Defective words, which were already marked up as such are highlighted in red. Words already annotated will be highlighted in yellow while annotation is awaiting review[1]. Hovering over a word with a mouse brings up a tooltip box displaying summarized information on that particular word. That will contain word's xml:id and all other attributes and their values (like lemma, pos or reg). Below attributes user would find a list of all annotations targeting that word. This tooltip will be read only and as it's connected with the word user is hovering over, it will change as user moves the mouse from one word to another.

[1] after annotation has been approved, the resulting changes are seamlessly merged into document's TEI source and no longer highlighted in the reading view, as we consider this an established encoding requiring no further attention. Record of that activity remains obviously available in the annotation list stored in the database, but interface for retrieving and displaying it is beyond scope for next weeks.

2. Creating editorial annotations - edit.png

Next step happens when the user finds a word which she wants to edit or comment on. Let's consider the former case first. Upon clicking on a word, the annotation panel on the right hand side is filled with an edit form as shown in edit.png wireframe.

a. single word selection - update
most typical case is when a single word was selected. Edit form in the annotation panel shows then the existing text content and all attribute values together with input fields allowing to change any of those. The list of attributes can be adjusted and wireframe shows just a few to illustrate the concept. In addition to word's content and attributes user can add a comment that relates to the edits she's made (so changes to the text content or attribute values). NB: it is not meant for exegetical annotations.

b. multiple word selection - join (join.png)
in case of multiple word selection it is assumed that the user aims to join words, which can be achieved by removing whitespace from the text content input field. Eventually (after approval of such an annotation) multiple <w> elements will be substituted with one, retaining the xml:id of the first selected token and attribute values from the edit form.

c. word split
splitting word happens in a similar manner - adding a whitespace into the text content creates a 'join' type annotation, which after approval results in a new <w> element being added to the TEI source document. NB: xml:ids for such newly created elements are right now arbitrary and quite ugly. We intend to prettify this at an export stage where it's easier to assure consistent numbering.

3. Exegetical annotations - comment.png
For adding longer annotations commenting on the selected text, user will need to switch to the 'Comment' tab in the annotation panel. A larger input box will appear together with an option for marking the annotation as 'private', so visible only for its author.

4. Layout - toc.png
Due to constraints in horizontal space available, we've decided to hide the navigation (TOC) panel by default.

5. Color-coding
As already noted, defective words and annotation status are characteristics of different objects -
first one of <w> elements and the second of annotations.
Thus only defects will be represented as @type attribute on <w> and the latter stored in the annotation record.

We have discussed two scenarios
a. user view
- defective words highlighted in red (perhaps with a red wavy underline as typical of text editors)
- words with pending annotations highlighted in yellow
As single word can be defective and have one or more annotations pending at the same time, it is necessary to differentiate the manner of highlighting (eg using border or underline and background color).

b. reviewer mode
Defective words are not really relevant here, while reviewer's attention should be brought to annotations awaiting review. Thus annotated words will be highlighted in yellow (following traffic lights colors). As reviewer accepts or rejects it, annotation highlight will change into green or red.

c. selection highlight
current selection is represented with a blue highlight, which will turn into yellow (pending annotation) or possibly also red (defective word) following creation of an annotation ticket.

6. Review process approve.png
Clicking on an annotated word brings up the edit form in the annotation panel. Reviewer is presented with Accept and Reject buttons, but in addition to that she might choose to fill the 'reviewer note' field.

7. Annotation model

Edit form in the annotation panel is a certain aggregation, allowing to edit distinct characteristics of a token on a single screen which Martin called an 'annotation ticket'. As every input field of the ticket represents an atomic action (eg regularization) it is stored internally as separate stand-off annotation.

Similarly rejecting or approving the ticket means in fact rejecting or approving all atomic annotations it consists of. Reviewer may also change some of the values proposed by the user. Such an act should be interpreted (and will be handled) as rejecting user's atomic annotation for that field and creating a new one (with reviewer as an author) with updated value.
