# Shakespearecontemporaries Installation and Configuration

## Prerequisites

* a current version of eXistdb (3.0 dev branch) should be installed and running
on the target system

## Installation

The shc application consists of 3 modules that need to be installed into eXistdb:

* shc.xar contains the webpages, the text transformation module (tei-publisher)
and is the front-facing application. This is where the users are managed and where
users will connect to to access the application.

* annotation-service.xar is the module responsible for handling the server-side
routines associated with annotation creation, searching and storage

* shctexts.xar is a data archive holding the texts that are accessed by shc. This will
create a collection of 'shctexts/data' that contains the actual TEI files. It's likely
that you'll want to manage this collection to add further files. 

## Configurations

### Email server

For the self-registration of users an email server is needed to send confirmations
when a user registered with the application. 

To configure the mail functionality please edit the file 'shc/components/xqmail/xqmail-config.xqm.

Following params must be set:

* $xqmail-config:FROM - represent the mail user connecting to the email server

* $xqmail-config:PASSWORD - the password to authenticate to the email server

* $xqmail-config:CC - optional cc address if you want to get copies of user registrations somewhere

* $xqmail-config:BCC - optional blind copies to be send to another address

Further down in the file a few more properties are defined for the email server configuration:

` <property name="mail.smtp.host" value="smtp.gmail.com"/>`

is the only one that needs to be changed appropriately. The other params should be self-explaining.

## shcadmin account

As there's no UI for the management of user accounts at least one shc admin (the role who can approve/reject proposed annotations) must be created manually.
The easiest way to do do is by using the Java admin client of eXistdb. You can run that client by executing:

`./bin/client.sh`

in the rootdir of the eXistdb installation.

This will bring up the client which lets you connect to the target server. In the toolbar you find an icon to manage users/groups.

The respective user account must be made member of the 'shcadmin' group.





