xquery version "3.1";

import module namespace config="http://www.tei-c.org/tei-simple/config" at "config.xqm";

declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace ep="http://earlyprint.org/ns/1.0";

declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";

let $id := request:get-parameter("id", ())
let $token := request:get-parameter("token", ())
let $work := doc($config:data-root || "/" || substring($id, 1, 3) || "/" || $id || ".xml")/tei:TEI

let $params :=
<output:serialization-parameters xmlns:output="http://www.w3.org/2010/xslt-xquery-serialization">
  <output:method value="xml"/>
  <output:version value="1.0"/>
  <output:media-type value="text/xml"/>
  <output:omit-xml-declaration value="yes"/>
  <output:encoding value="UTF-8"/>
</output:serialization-parameters>

return
    (
        response:set-cookie("simple.token", $token),
        response:set-header("Content-Disposition",
            concat("attachment; filename=", concat($id, '.xml'))),
        response:stream
        (
            concat
            (
            (:  The serializer provides no way to include the xml-model processing instruction so we
                have to prepend that ourselves, and that has to come after the xml declaration, so we have
                to suppress the xml declaration and prepend one of those too.
            :)
                '<?xml version="1.0" encoding="UTF-8"?>',
                '&#xa;',
                '<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>',
                '&#xa;',
                fn:serialize($work, $params)
            ) ,
            "method=text media-type=text/xml" 
        )
     )
