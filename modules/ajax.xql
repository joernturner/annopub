(:
 : Copyright 2015, Wolfgang Meier
 :
 : This software is dual-licensed:
 :
 : 1. Distributed under a Creative Commons Attribution-ShareAlike 3.0 Unported License
 : http://creativecommons.org/licenses/by-sa/3.0/
 :
 : 2. http://www.opensource.org/licenses/BSD-2-Clause
 :
 : All rights reserved. Redistribution and use in source and binary forms, with or without
 : modification, are permitted provided that the following conditions are met:
 :
 : * Redistributions of source code must retain the above copyright notice, this list of
 : conditions and the following disclaimer.
 : * Redistributions in binary form must reproduce the above copyright
 : notice, this list of conditions and the following disclaimer in the documentation
 : and/or other materials provided with the distribution.
 :
 : This software is provided by the copyright holders and contributors "as is" and any
 : express or implied warranties, including, but not limited to, the implied warranties
 : of merchantability and fitness for a particular purpose are disclaimed. In no event
 : shall the copyright holder or contributors be liable for any direct, indirect,
 : incidental, special, exemplary, or consequential damages (including, but not limited to,
 : procurement of substitute goods or services; loss of use, data, or profits; or business
 : interruption) however caused and on any theory of liability, whether in contract,
 : strict liability, or tort (including negligence or otherwise) arising in any way out
 : of the use of this software, even if advised of the possibility of such damage.
 :)
xquery version "3.0";

import module namespace functx = "http://www.functx.com";

declare namespace output="http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace ep="http://earlyprint.org/ns/1.0";

import module namespace console="http://exist-db.org/xquery/console" at "java:org.exist.console.xquery.ConsoleModule";
import module namespace app="http://www.tei-c.org/tei-simple/templates" at "app.xql";
import module namespace pages="http://www.tei-c.org/tei-simple/pages" at "pages.xql";
import module namespace config="http://www.tei-c.org/tei-simple/config" at "config.xqm";
import module namespace annotation="http://existsolutions.com/annotation-service/annotation" at "../../annotation-service/modules/annotation.xql";

declare boundary-space strip;

declare option output:method "json";
declare option output:media-type "application/json";


(:
 : This module is called from Javascript when the user wants to navigate to the next/previous
 : page.
 :)
let $user := app:current-user-and-role()
let $doc := request:get-parameter("doc", ())
let $x := console:log("Loading doc " || $doc)


let $root := request:get-parameter("root", ())
let $id := request:get-parameter("id", ())
let $view := request:get-parameter("view", $config:default-view)
let $xml :=
    if ($id) then (
        console:log("Loading by id " || $id),
        let $node := doc($config:data-root || "/" || substring($doc, 1, 3) || "/" || $doc)/id($id)
        let $div := $node/ancestor-or-self::tei:div[1]
        return
            if (empty($div)) then
                $node/following-sibling::tei:div[1]
            else
                $div
    ) else
        let $page := request:get-parameter("page", ())
        let $root :=
            if ($page) then
                let $x := console:log("ajax.xql: Loading by page " || $page)
                let $docname := $config:data-root || "/" || substring($doc, 1, 3) || "/" || $doc
                let $pb := doc($docname)//id(functx:substring-before-last($doc, '.') || '-' ||$page)
                let $pb :=  if ($pb)
                            then $pb
                            else
                                doc($docname)//id(functx:substring-before-last($doc, '_') || '-' || $page)

                let $nodeid := util:node-id($pb)
                return $nodeid
            else $root
        let $x := console:log("Loading root " || $root)
        return
        pages:load-xml($view, $root, $doc)

return
    if ($xml) then
        let $prev :=
            switch ($view)
                case "page" return
                    $xml/preceding::tei:pb[not(@type) or (@type != 'blank' and @type !='duplicate')][1]
                default return
                    let $parent := $xml/ancestor::tei:div[not(*[1] instance of element(tei:div))][1]
                    let $prevDiv := $xml/preceding::tei:div[1]
                    return
                        pages:get-previous(if ($parent and (empty($prevDiv) or $xml/.. >> $prevDiv)) then $xml/.. else $prevDiv)
        let $next :=
            switch ($view)
                case "page" return
                    $xml/following::tei:pb[not(@type) or (@type != 'blank' and @type !='duplicate')][1]
                default return
                    pages:get-next($xml)
        let $data := pages:get-content(root($xml), $xml)
        let $content :=
        if ($data/tei:pb and count($data/*) = 1) then
            element { node-name($data) } {
                $data/@* except $data/@rend,
                attribute rend { "empty-page" },
                $data/node()
            }
        else
            $data
        (: let $start := util:system-time() :)
        let $html := pages:process-content($content, $xml)
        (: let $time := console:log("process-content took " || (util:system-time() - $start)) :)
        let $doc := replace($doc, "^.*/([^/]+)$", "$1")
        let $rootNode := root($xml)/tei:TEI
        let $annotations := annotation:get-annotations(
                                $rootNode/@xml:id/string(),
                                $rootNode/@n/string(),
                                $content,
                                pages:get-annotation-filters-for($user)
                            )
        let $viewparam := if ($view eq $config:default-view)
                          then ""
                          else "&amp;view=" || $view
        return
            map {
                "doc": $doc,
                "odd": $config:odd,
                "next":
                    if ($next) then
                        $doc
                        || (if ($view eq "page")
                            then "?page=" || substring-after($next/@xml:id, '-')
                            else "?root=" || util:node-id($next))
                        || $viewparam
                    else (),
                "previous":
                    if ($prev) then
                        $doc
                        || (if ($view eq "page")
                            then "?page=" || substring-after($prev/@xml:id, '-')
                            else "?root=" || util:node-id($prev))
                        || $viewparam
                    else (),
                "switchView":
                    let $root := pages:switch-view-id($xml, $view)
                    return
                        if ($root) then
                            $doc
                            || (if ($view = "page")
                                then "?root=" || util:node-id($root)
                                else "?page=" || substring-after($root/@xml:id, '-'))
                            || (if ($view = "div") then "" else "&amp;view=div")
                        else
                            (),
                "content": serialize($html,
                    <output:serialization-parameters xmlns:output="http://www.w3.org/2010/xslt-xquery-serialization">
                      <output:indent>no</output:indent>
                      <output:method>html</output:method>
                      <output:version>5.0</output:version>
                    </output:serialization-parameters>),
                "annotations": $annotations
            }
    else if ($doc eq "review.html") then

        let $annotations := app:get-page-annotations()
        return
            map {
                "doc": $doc,
                "annotations": $annotations
            }

    else
        map { "error": "Not found" }
