xquery version "3.1";

import module namespace config="http://www.tei-c.org/tei-simple/config" at "config.xqm";
import module namespace console="http://exist-db.org/xquery/console" at "java:org.exist.console.xquery.ConsoleModule";
import module namespace memsort="http://exist-db.org/xquery/memsort" at "java:org.existdb.memsort.SortModule";
import module namespace memsort_indexes="http://texts.earlyprint.org/memsort_indexes" at "memsort_indexes.xql";


declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace ep="http://earlyprint.org/ns/1.0";

declare function local:index() {
    for $doc in collection($config:data-root)//tei:TEI

    let $titleStmt :=
    (
        $doc//tei:fileDesc/tei:titleStmt
    )

    let $keywords :=
    (
        $doc//tei:profileDesc/tei:textClass/tei:keywords
    )

    let $curators :=
    (
        $doc//ep:curator/ep:name
    )

    let $proofreaders :=
    (
        $doc//ep:proofReader/ep:name
    )

    (: much faster than $doc/@xml:id :)
    let $docid := substring-before(util:document-name($doc), '.xml')
    let $identifiers :=
    (
        $docid,                             (: e.g., A04632_09 :)
        if ($doc//ep:tcp/text() != $docid)
        then $doc//ep:tcp/text()            (: e.g., A04632 :)
        else (),
        $doc//ep:stc/text(),
        $doc//ep:estc/text()
    )

    let $grade := $doc//ep:finalGrade/text()

    let $haspageimages := if ( count($doc//tei:facsimile) >0 ) then "1" else "0"

    let $pcount := ($doc//ep:pageCount/text())[1]

    let $pagecount :=
        if ( string-length( $pcount ) ) then
            format-number( xs:integer( $pcount ) , "#" )
        else
            format-number( count( $doc//tei:pb ) , "#" )

    let $wcount := ($doc//ep:wordCount/text())[1]

    let $wordcount :=
        if ( string-length( $wcount ) ) then
            format-number( xs:integer( $wcount ) , "#" )
        else
            format-number( count( $doc//tei:w ) , "#" )

    let $corpora :=
    (
        $doc//ep:corpus/text(),
        if ($doc//ep:corpus/text() = 'shc')
        then 'drama'
        else ()
    )

    let $year :=
    (
        if ( string-length( $doc//ep:creationYear/text() ) ) then
            $doc//ep:creationYear/text()
        else
            $doc//ep:publicationYear/text()
    )

    let $index :=
        <doc>
            {
                for $title in $titleStmt/tei:title
                return
                    <field name="title" store="yes">{string-join($title/text(), " ")}</field>
            }

            {
                for $author in $titleStmt/tei:author
                let $normalized := replace($author/text(), "^([^,]*,[^,]*),?.*$", "$1")
                return
                    <field name="author" store="yes">{$normalized}</field>
            }

            {
                for $curator in $curators
                let $normalized := replace($curator/text(), "^([^,]*,[^,]*),?.*$", "$1")
                return
                    <field name="curator" store="yes">{$normalized}</field>
            }

            {
                for $proofreader in $proofreaders
                let $normalized := replace($proofreader/text(), "^([^,]*,[^,]*),?.*$", "$1")
                return
                    <field name="proofreader" store="yes">{$normalized}</field>
            }

            {
                for $term in $keywords/tei:term
                return
                    <field name="keyword" store="yes">{string-join($term/text(), " ")}</field>
            }

            <field name="year" store="yes">{$doc/tei:teiHeader/tei:fileDesc/tei:editionStmt/tei:edition/tei:date/text()}</field>
            <field name="date" store="yes">{$year}</field>
            <field name="genre" store="yes">{$doc//ep:subgenre/text()}</field>

            {
                for $identifier in $identifiers
                return
                    <field name="identifier" store="yes">{$identifier}</field>
            }

            <field name="grade" store="yes">{$grade}</field>
            <field name="haspageimages" store="yes">{$haspageimages}</field>
            <field name="pagecount" store="yes">{$pagecount}</field>
            <field name="wordcount" store="yes">{$wordcount}</field>
            {
                for $corpus in $corpora
                return
                    <field name="corpus" store="yes">{string-join($corpus, " ")}</field>
            }
        </doc>
    return
        ft:index(document-uri(root($doc)), $index)
};

declare function local:clear() {
    for $doc in collection($config:data-root)//tei:TEI
    return
        ft:remove-index(document-uri(root($doc)))
};


try {
    let $start-time := util:system-time()
    let $x := local:clear()
    let $x := local:index()
    let $metadata_seconds := (util:system-time() - $start-time) div xs:dayTimeDuration("PT1S")
    let $start-time := util:system-time()
    let $x := memsort_indexes:generate-memsort-indexes()
    let $memsort_seconds := (util:system-time() - $start-time) div xs:dayTimeDuration("PT1S")

    return
        <p>Successfully created metadata indexes in {$metadata_seconds} seconds and in-memory indexes in {$memsort_seconds} seconds!</p>
}
catch * {
    <div class="list-group-item-danger">
        <h5 class="list-group-item-heading">Document metadata index update failed.</h5>
        <p class="list-group-item-text">{ $err:description }</p>
    </div>
}
