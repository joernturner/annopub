xquery version "3.1";

import module namespace memsort_indexes="http://texts.earlyprint.org/memsort_indexes" at "memsort_indexes.xql";

try {
    let $start-time := util:system-time()
    let $start-time := util:system-time()
    let $x := memsort_indexes:generate-memsort-indexes()
    let $memsort_seconds := (util:system-time() - $start-time) div xs:dayTimeDuration("PT1S")

    return
        <p>Successfully created in-memory indexes in {$memsort_seconds} seconds!</p>
}
catch * {
    <div class="list-group-item-danger">
        <h5 class="list-group-item-heading">In-memory index update failed.</h5>
        <p class="list-group-item-text">{ $err:description }</p>
    </div>
}