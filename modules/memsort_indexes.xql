xquery version "3.1";
(:~
 : Generate indexes for sorting the main list of texts.  Using memsort
 : makes the order by many times faster than it would otherwise be.
 :)
module namespace memsort_indexes="http://texts.earlyprint.org/memsort_indexes";

import module namespace config="http://www.tei-c.org/tei-simple/config" at "/db/apps/shc/modules/config.xqm";
import module namespace console="http://exist-db.org/xquery/console" at "java:org.exist.console.xquery.ConsoleModule";
import module namespace memsort="http://exist-db.org/xquery/memsort" at "java:org.existdb.memsort.SortModule";

declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace ep="http://earlyprint.org/ns/1.0";

declare function memsort_indexes:generate-memsort-indexes() {

    let $doc_collection := collection($config:data-root)

    return
    let $index := memsort:create(
        "memsort-author",
        $doc_collection//tei:TEI,
        function($tei) {
            string-join($tei//ep:author/ep:name, "; ")
        }
    )

    let $index := memsort:create(
        "memsort-date",
        $doc_collection//tei:TEI,
        function($tei) {
            let $date := ($tei//ep:creationYear/text(),
                          $tei//ep:publicationYear/text(),
                          $tei//tei:biblFull[@n="printed source"]//tei:date[@type="creation_date"]/text(),
                          $tei//tei:date/text()
                        )[1]
            return $date
        }
    )

    let $index := memsort:create(
        "memsort-title",
        $doc_collection//tei:TEI,
        function($tei) {
            let $title := ($tei//ep:title/text(),
                           $tei//tei:title[@type = 'main']/text(),
                           $tei//tei:title/text()
                          )[1]

            let $title :=  fn:lower-case(fn:replace($title , "['\.;,&quot;&amp;\-\]\[\(\)]+", ""))
            let $toktitle := fn:tokenize( $title , "\s+" )
            (: Move numbers to the end :)
            let $firsttok := $toktitle[1]
            let $toktitle :=
                if ( fn:string(fn:number($firsttok)) != 'NaN' ) then
                    fn:insert-before(fn:remove($toktitle, 1), 999, $firsttok)
                else
                   $toktitle
            (: Move a/an/the to the end :)
            let $firsttok := $toktitle[1]
            let $toktitle :=
                if ($firsttok = ("", "a", "an", "the")) then
                    fn:insert-before(fn:remove($toktitle, 1), 999, $firsttok)
                else
                   $toktitle

            let $title := fn:string-join($toktitle, " ")

            return
            (: We could have more than one text with the same title, but it must be unique for sorting
             : purposes, so tack on an unique identifier.
             :)
                $title || util:uuid()
        }
    )

    let $index := memsort:create(
        "memsort-grade",
        $doc_collection//tei:TEI,
        function($tei) {
            $tei//ep:finalGrade/text()
        }
    )

    let $index := memsort:create(
        "memsort-pagecount",
        $doc_collection//tei:TEI,
        function($tei) {
            xs:integer(ft:get-field(document-uri(root($tei)), "pagecount" ))
        }
    )

    let $index := memsort:create(
        "memsort-wordcount",
        $doc_collection//tei:TEI,
        function($tei) {
            xs:integer(ft:get-field(document-uri(root($tei)), "wordcount" ))
        }
    )

    return ()
};

