xquery version "3.1";

import module namespace config="http://www.tei-c.org/tei-simple/config" at "config.xqm";
import module namespace console="http://exist-db.org/xquery/console" at "java:org.exist.console.xquery.ConsoleModule";
import module namespace epub="http://exist-db.org/xquery/epub" at "epub.xql";
import module namespace http="http://expath.org/ns/http-client" at "java:org.exist.xquery.modules.httpclient.HTTPClientModule";

declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace ep="http://earlyprint.org/ns/1.0";
declare namespace jmx="http://exist-db.org/jmx";

declare option exist:serialize "method=xml media-type=text/xml";

(: Try to dynamically determine fonts directory by calling JMX. :)
declare function local:get-fonts-dir() {
    try {
    let $request := <http:request method="GET" href="http://localhost:{request:get-server-port()}/{request:get-context-path()}/status?c=disk"/>
    let $response := http:send-request($request)
    return
        if ($response[1]/@status = "200") then
            let $dataDir := $response[2]//jmx:DataDirectory/string()
            let $expath := $config:expath-descriptor
            let $pkgRoot := $expath/@abbrev || "-" || $expath/@version
            return
                $dataDir || "/expathrepo/" || $pkgRoot || "/resources/fonts"
        else
            ()
        } catch * {
            ()
        }
};

declare function local:work2epub($id as xs:string, $work as element()) {
    let $root := $work/ancestor-or-self::tei:TEI
    let $fileDesc := $root/tei:teiHeader/tei:fileDesc
    let $fontsDir := local:get-fonts-dir()
    let $config := map {
        "metadata": map {
            "title": $fileDesc/tei:titleStmt/tei:title/string(),
            "creator": $fileDesc/tei:titleStmt/tei:author/string(),
            "urn": util:uuid(),
            "language": "en"
        },
        "odd": $config:odd,
        "output-root": $config:odd-root
    }
    let $oddName := replace($config:odd, "^([^/\.]+).*$", "$1")
    let $cssDefault := util:binary-to-string(util:binary-doc($config:output-root || "/" || $oddName || ".css"))
    let $cssEpub := util:binary-to-string(util:binary-doc($config:app-root || "/resources/css/epub.css"))
    let $css := $cssDefault ||
        "&#10;/* styles imported from epub.css */&#10;" ||
        $cssEpub
    return
        epub:generate-epub($config, $root, $css, $id)
};

let $id := request:get-parameter("id", ())
let $token := request:get-parameter("token", ())
let $useCache := request:get-parameter("cache", "no")
let $standardize := request:get-parameter("standardize", ())
let $origwork := doc( $config:data-root || "/" || substring($id, 1, 3) || "/" || $id || ".xml" )
let $xslURI := $config:app-root || "/resources/xsl/remove_w_and_pc.xsl"
let $xslParams :=
    if ( $standardize = "true" ) then
    (
        <parameters>
            <param name="standardize" value="true" />
        </parameters>
    )
    else
    (
        <parameters>
            <param name="standardize" value="false" />
        </parameters>
    )

            (: Remove <w> and <pc> elements. :)

let $work :=
    fn:serialize
    (
        transform:transform
        (
            $origwork,
            doc( $xslURI ),
            $xslParams
        )
    )

           (: Ensure space immediately before and/or after tag is not lost. :)

let $work := replace( $work , " <" , "<c> </c><" )
let $work := replace( $work , "> " , "><c> </c>" )

            (: Parse generated XML. :)

let $work := fn:parse-xml( $work )/tei:TEI

            (: Convert text to epub. :)

let $entries := local:work2epub( $id , $work )

return
    (
        response:set-cookie("simple.token", $token),
        response:set-header("Content-Disposition", concat("attachment; filename=", concat($id, '.epub'))),
        response:stream-binary(
            compression:zip( $entries, true() ),
            'application/epub+zip',
            concat($id, '.epub')
        )
    )
