xquery version "3.0";

declare namespace output="http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace fo="http://www.w3.org/1999/XSL/Format";
declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace ep="http://earlyprint.org/ns/1.0";
declare namespace jmx="http://exist-db.org/jmx";

declare option exist:serialize "method=text media-type=text/plain omit-xml-declaration=yes indent=no";

import module namespace config="http://www.tei-c.org/tei-simple/config" at "config.xqm";
import module namespace pm-config="http://www.tei-c.org/tei-simple/pm-config" at "pm-config.xql";
import module namespace console="http://exist-db.org/xquery/console" at "java:org.exist.console.xquery.ConsoleModule";
import module namespace process="http://exist-db.org/xquery/process" at "java:org.exist.xquery.modules.process.ProcessModule";
import module namespace pmu="http://www.tei-c.org/tei-simple/xquery/util";
import module namespace odd="http://www.tei-c.org/tei-simple/odd2odd";
import module namespace xslfo="http://exist-db.org/xquery/xslfo" at "java:org.exist.xquery.modules.xslfo.XSLFOModule";
import module namespace http="http://expath.org/ns/http-client" at "java:org.exist.xquery.modules.httpclient.HTTPClientModule";
import module namespace s2i="http://existsolutions.com/annotation-service/standoff2inline" at "../../annotation-service/modules/standoff2inline.xqm";
import module namespace file="http://exist-db.org/xquery/file" at "java:org.exist.xquery.modules.file.FileModule";

declare variable $local:WORKING_DIR := local:get-fonts-dir() || "../";
(:  Set to 'ah' for AntennaHouse, 'fop' for Apache fop :)
declare variable $local:PROCESSOR := "fop";

declare variable $local:CACHE := true();

declare variable $local:CACHE_COLLECTION := $config:app-root || "/cache";

declare function local:prepare-cache-collection() {
    if (xmldb:collection-available($local:CACHE_COLLECTION)) then
        ()
    else
        (xmldb:create-collection($config:app-root, "cache"))[2]
};

declare function local:fop($id as xs:string, $fontsDir as xs:string?, $fo as element()) {
    let $config :=
        <fop version="1.0">
            <!-- Strict user configuration -->
            <strict-configuration>true</strict-configuration>

            <!-- Strict FO validation -->
            <strict-validation>false</strict-validation>

            <!-- Base URL for resolving relative URLs -->
            <base>./</base>

            <renderers>
                <renderer mime="application/pdf">
                    <fonts>
                    {
                        if ($fontsDir) then (
                            <font kerning="yes"
                                embed-url="file:{$fontsDir}/Junicode.ttf"
                                encoding-mode="cid">
                                <font-triplet name="Junicode" style="normal" weight="normal"/>
                            </font>,
                            <font kerning="yes"
                                embed-url="file:{$fontsDir}/Junicode-Bold.ttf"
                                encoding-mode="cid">
                                <font-triplet name="Junicode" style="normal" weight="700"/>
                            </font>,
                            <font kerning="yes"
                                embed-url="file:{$fontsDir}/Junicode-Italic.ttf"
                                encoding-mode="cid">
                                <font-triplet name="Junicode" style="italic" weight="normal"/>
                            </font>,
                            <font kerning="yes"
                                embed-url="file:{$fontsDir}/Junicode-BoldItalic.ttf"
                                encoding-mode="cid">
                                <font-triplet name="Junicode" style="italic" weight="700"/>
                            </font>,
                            <font kerning="yes"
                                embed-url="file:{$fontsDir}/CardoRegular.ttf"
                                encoding-mode="cid">
                               <font-triplet name="Cardo" style="normal" weight="normal"/>
                            </font>,
                            <font kerning="yes"
                                 embed-url="file:{$fontsDir}/CardoItalic.ttf"
                                 encoding-mode="cid">
                                <font-triplet name="Cardo" style="italic" weight="normal"/>
                            </font>,
                           <font kerning="yes"
                                embed-url="file:{$fontsDir}/CardoBold.ttf"
                                encoding-mode="cid">
                                <font-triplet name="Cardo" style="normal" weight="700"/>
                            </font>,
                            <font kerning="yes"
                                embed-url="file:{$fontsDir}/unifont.ttf"
                                encoding-mode="cid">
                                <font-triplet name="unifont" style="normal" weight="normal"/>
                            </font>,
                            <substitutions>
                                <substitution>
                                   <from font-family="Helvetica"/>
                                   <to font-family="Junicode"/>
                                </substitution>
                                <substitution>
                                   <from font-family="Arial"/>
                                   <to font-family="Junicode"/>
                                </substitution>
                            </substitutions>
                        ) else
                            ()
                    }
                    </fonts>
                </renderer>
            </renderers>
        </fop>

(: Clean up generated fop. :)

let $myfo := replace( serialize( $fo ), "\n[\s]*<" , "<" )
let $myfo := replace( $myfo, "<fo:external-graphic.*?</fo:external-graphic>" , "" )
let $myfo := replace( $myfo, "<fo:inline>https://.*?</fo:inline>" , "" )
let $myfo := replace( $myfo, "Verdana, Tahoma, Geneva, Arial, Helvetica, sans-serif", "Junicode, Cardo, unifont" )
let $myfo := replace( $myfo, "<fo:inline" , " <fo:inline" )
let $myfo := replace( $myfo, "</fo:inline" , " </fo:inline" )
let $myfo := replace( $myfo, "<fo:flow(.*?)>" , "<fo:flow$1><fo:block>" )
let $myfo := replace( $myfo, "</fo:flow>" , "</fo:block></fo:flow>" )
let $myfo :=
    replace( $myfo, '<fo:marker marker-class-name="heading">.*?</fo:marker>' ,
                "" )
let $myfo := '<?xml version="1.0" encoding="utf-8"?>' || $myfo
let $myfo := replace( $myfo, 'font-family="Junicode"' , "" )
let $myfo := parse-xml( $myfo )
let $pdf := xslfo:render($myfo, "application/pdf", (), $config)
return $pdf
};

declare function local:antenna-house($id as xs:string, $fo as element()) {
    let $file :=
        $local:WORKING_DIR || "/" || encode-for-uri($id) ||
        format-dateTime(current-dateTime(), "-[Y0000][M00][D00]-[H00][m00]") || "-" || request:get-remote-addr()
    let $serialized := file:serialize($fo, $file || ".fo", "indent=no")
    let $options :=
        <option>
            <workingDir>{system:get-exist-home()}</workingDir>
        </option>
    let $result := (
        process:execute(
            (
                "sh", "/usr/AHFormatterV6_64/run.sh", "-d", $file || ".fo", "-o", $file || ".pdf", "-x", "2",
                "-peb", "1", "-pdfver", "PDF1.6",
                "-p", "@PDF",
                "-tpdf"
            ), $options
        )
    )
    return (
        if ($result/@exitCode = 0) then
            let $pdf := file:read-binary($file || ".pdf")
            return
                $pdf
        else
            $result
    )
};

declare function local:cache($id as xs:string, $output as xs:base64Binary) {
    local:prepare-cache-collection(),
    xmldb:store($local:CACHE_COLLECTION, $id || ".pdf", $output, "application/pdf")
};

declare function local:get-cached($id as xs:string, $doc as element(tei:TEI)) {
    let $path := $local:CACHE_COLLECTION || "/" ||  $id || ".pdf"
    return
        if ($local:CACHE and util:binary-doc-available($path)) then
            let $modDatePDF := xmldb:last-modified($local:CACHE_COLLECTION, $id || ".pdf")
            let $modDateSrc := xmldb:last-modified(util:collection-name($doc), util:document-name($doc))
            return
                if ($modDatePDF >= $modDateSrc) then
                    util:binary-doc($path)
                else
                    ()
        else
            ()
};

(: Try to dynamically determine fonts directory by calling JMX. :)
declare function local:get-fonts-dir() {
    try {
    let $request := <http:request method="GET" href="http://localhost:{request:get-server-port()}/{request:get-context-path()}/status?c=disk"/>
    let $response := http:send-request($request)
    return
        if ($response[1]/@status = "200") then
            let $dataDir := $response[2]//jmx:DataDirectory/string()
            let $expath := $config:expath-descriptor
            let $pkgRoot := $expath/@abbrev || "-" || $expath/@version
            return
                $dataDir || "/expathrepo/" || $pkgRoot || "/resources/fonts"
        else
            ()
        } catch * {
            ()
        }
};

let $path := request:get-parameter("doc", ())
let $token := request:get-parameter("token", "none")
let $source := request:get-parameter("source", ())
let $standardize := request:get-parameter("standardize", ())
let $useCache := request:get-parameter("cache", "no")
let $origdoc := doc($config:data-root || "/" || substring($path, 1, 3) || "/" || $path)

let $xslURI := $config:app-root || "/resources/xsl/remove_w_and_pc_for_fop.xsl"

let $xslParams :=
    if ( $standardize = "true" ) then
    (
        <parameters>
            <param name="standardize" value="true" />
        </parameters>
    )
    else
    (
        <parameters>
            <param name="standardize" value="false" />
        </parameters>
    )

(: Remove <w> and <pc> elements. :)

let $doc :=
fn:serialize
(
    transform:transform
    (
        $origdoc,
        doc( $xslURI ),
        $xslParams
    )
)

let $doc := fn:parse-xml( $doc )/tei:TEI
let $id := replace($path, "^(.*)\..*", "$1")
let $name := util:document-name($origdoc/tei:TEI)
let $doc := s2i:merge($doc, ())

return
    if ($doc) then
        let $cached := if ($useCache = ("yes", "true")) then local:get-cached($name, $doc) else ()
        return (
            response:set-cookie("simple.token", $token),
            if (not($source) and exists($cached)) then (
                response:stream-binary($cached, "media-type=application/pdf", $id || ".pdf")
            ) else
                let $start := util:system-time()
                let $fo := $pm-config:print-transform($doc, (), ())

                return (
                    if ($source) then
                        $fo
                    else
                        let $output :=
                            switch ($local:PROCESSOR)
                                case "ah" return
                                    local:antenna-house($name, $fo)
                                default return
                                    local:fop($name, local:get-fonts-dir(), $fo)
                        return
                            typeswitch($output)
                                case xs:base64Binary return (
                                    let $path := local:cache($name, $output)
                                    return
                                        response:stream-binary(util:binary-doc($path), "media-type=application/pdf", $id || ".pdf")
                                )
                                default return
                                    $output
                )
        )
    else
        ()
