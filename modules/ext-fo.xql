xquery version "3.1";

module namespace pmf="http://pib.at.northwestern.edu/apps/shc/fo-behaviours";

declare namespace tei="http://www.tei-c.org/ns/1.0";

import module namespace fof="http://www.tei-c.org/tei-simple/xquery/functions/fo";

declare function pmf:characters($content as item()) {
    typeswitch($content)
        case text() return
            if (matches($content, "\s+")) then
                ()
            else
                fof:escapeChars($content)
        default return
            fof:escapeChars($content)
};

declare function pmf:text($config as map(*), $node as element(), $class as xs:string+, $content as item()*) {
    $content/string() ! fof:escapeChars(.)
};