(:~
 : Transform a given source into a standalone document using
 : the specified odd.
 :
 : @author Wolfgang Meier
 : @author Philip R. Burns.   Modifications for Early Print project.
 :)
xquery version "3.0";

declare namespace output="http://www.w3.org/2010/xslt-xquery-serialization";

import module namespace config="http://www.tei-c.org/tei-simple/config" at "config.xqm";
import module namespace console="http://exist-db.org/xquery/console" at "java:org.exist.console.xquery.ConsoleModule";
import module namespace pmu="http://www.tei-c.org/tei-simple/xquery/util";
import module namespace odd="http://www.tei-c.org/tei-simple/odd2odd";
declare namespace tei="http://www.tei-c.org/ns/1.0";

declare option output:method "html";
declare option output:html-version "5.0";
declare option output:media-type "text/html";

let $doc := request:get-parameter("doc", ())
let $odd := request:get-parameter("odd", $config:odd)
let $standardize := request:get-parameter("standardize", ())
let $token := request:get-parameter("token", "none")


return
    if ( $doc ) then
        let $odd := $config:odd-root || "/" || $odd
        let $xml := $config:data-root || "/" || substring($doc, 1, 3) || "/" || $doc
        let $xslURI := $config:app-root || "/resources/xsl/singlepage_html.xsl"
        let $xslParams :=
            if ( $standardize = "true" ) then
            (
                <parameters>
                    <param name="standardize" value="true" />
                </parameters>
            )
            else
            (
                <parameters>
                    <param name="standardize" value="false" />
                </parameters>
            )

            (: Format text using current odd specifications. :)

            let $work :=
                fn:serialize
                (
                    transform:transform
                    (
                        doc( $xml ),
                        doc( $xslURI ),
                        $xslParams
                    )
                )

           (: Ensure space immediately before or after tag is not lost. :)

        let $work := replace( $work , " <" , "<c> </c><" )
        let $work := replace( $work , "> " , "><c> </c>" )

        (: Parse generated XML. :)

        let $work := fn:parse-xml( $work )/tei:TEI

        return (
            response:set-cookie("simple.token", $token),
            pmu:process( $odd, $work, $config:output-root, "web",
                         "../" || $config:output, $config:module-config,
                         map { "root": $work }
                       )
        )
    else
        <p>No document specified</p>