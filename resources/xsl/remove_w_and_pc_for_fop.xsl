<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns="http://www.tei-c.org/ns/1.0"
    xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs tei" version="2.0">

    <!-- Retrieve standardize parameter.-->

    <xsl:param name="standardize"/>

    <!-- Suppress excess spaces. -->

    <xsl:strip-space elements="*"/>

    <!-- Default identity templates -->

    <xsl:template match="element()">
        <xsl:copy>
            <xsl:apply-templates select="@*, node()"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="attribute() | text() | comment() | processing-instruction()">
        <xsl:copy/>
    </xsl:template>

    <!-- Eject  machine-generated_castlist and
            textual_notes divs.
    -->

    <xsl:template match="tei:div">
        <xsl:choose>
            <xsl:when test="@type='machine-generated_castlist' or @type= 'textual_notes'">
            </xsl:when>
            <xsl:otherwise>
                <xsl:copy>
                    <xsl:apply-templates select="@*, node()"/>
                </xsl:copy>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- Eject facsimile section. -->

    <xsl:template match="tei:facsimile">
    </xsl:template>

    <!-- If <pc> has @join='right' or @join='both',  add space before it. -->

    <xsl:template match="tei:pc">
        <xsl:if test="@join='right' or @join='both'">
            <xsl:text> </xsl:text>
        </xsl:if>
        <xsl:apply-templates select="text()" />
    </xsl:template>

    <!-- If <w> is preceded by <pc> with @join='right' or @join='both', copy with no leading space -->

    <xsl:template match="tei:w">
        <xsl:choose>
            <xsl:when test="preceding::*[1][self::tei:pc[@join='right' or @join='both']]">
            </xsl:when>
            <xsl:when test="preceding::*[1][self::tei:w[@join='right' or @join='both']]">
            </xsl:when>
            <xsl:when test="@join='left' or @join='both'">
            </xsl:when>
            <xsl:otherwise>
                <xsl:text> </xsl:text>
            </xsl:otherwise>
        </xsl:choose>
       <xsl:choose>
            <xsl:when test="@reg and $standardize='true'">
                 <w><xsl:value-of select="@reg"/></w>
            </xsl:when>
            <xsl:otherwise>
                <w><xsl:value-of select="text()"/></w>
            </xsl:otherwise>
        </xsl:choose>
     </xsl:template>
</xsl:stylesheet>
