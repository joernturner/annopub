$(document).ready(function () {
    var historySupport = !!(window.history && window.history.pushState);
    var appRoot = $("html").data("app");

    initTooltip();

    // SPELLING //

    var showStandardSpellings = false;

    function setSpelling() {
        var nodeList = document.querySelectorAll('tei-w[orig]');

        for (var l = 0; l < nodeList.length; l++) {
            var w = nodeList[l];
            if (showStandardSpellings) {
                w.innerHTML = w.getAttribute('reg');
            }
            else {
                w.innerHTML = w.getAttribute('orig');
            }
        }
    }

    // PAGE NAVIGATION //

    var toPreviousPageButton = $('.nav-prev');
    var toNextPageButton = $('.nav-next');

    function setNavigationButtonState(button, link) {
        if (!link) {
            return button.removeAttr('href').attr("disabled", true);
        }
        button.attr("href", link).removeAttr("disabled");
    }

    $('.viewimage').on("click", function (e) {
        e.preventDefault();
        var facs = $('#facsimile');
        if (facs.hasClass("hidden")) {
            facs.removeClass("hidden");
            $(this).addClass("on");
            $(this).text("Hide images");
        }
        else {
            facs.addClass("hidden");
            $(this).removeClass("on");
            $(this).text("Show images");
        }

    });

    function resize() {
        // $("#content-container").each(function() {
        //     var wh = $(window).height();
        //     var ot = $(this).offset().top;
        //     $(this).height(wh - ot);
        //     $("#image-container").height(wh - ot);
        // });
    }

    function getFontSize() {
        var size = $("#content-inner").css("font-size");
        return parseInt(size.replace(/^(\d+)px/, "$1"));
    }

    /* Keep either Browse or Review highlighted if one of
     * them has been selected.
     */
    var browseLink = document.getElementById("texts");
    var reviewLink = document.getElementById("review");

    if (window.location.pathname.includes('/works/')) {
        if (browseLink) browseLink.classList.add("menu-highlight");
        if (reviewLink) reviewLink.classList.remove("menu-highlight");
    }
    if (window.location.pathname.includes('/review/')) {
        if (browseLink) browseLink.classList.remove("menu-highlight");
        if (reviewLink) reviewLink.classList.add("menu-highlight");
    }

    // FACSIMILE //

    var facsimileViewer = document.getElementById('facsimile')
    var screenshotLoading = document.getElementById('screenshot_loading')

    function getFacsimileUrl() {
        var imageSrcElement = $('[data-facs]');
        if(!imageSrcElement){ return null}
        var facsData = imageSrcElement.data("facs");
        return facsData ? facsData.trim() : null;
    }

    function loadFacsimile(facsimileUrl) {
        if (!facsimileViewer) {
            return
        }
        if (!facsimileUrl) {
            hideViewer()
            return console.log('FacsimileViewer:', 'no facsimile url')
        }
        if (facsimileUrl.endsWith("9999")) {
            facsimileViewer.innerHTML = '<div class="wrapper">'
                                        + '<p/><h4  style="text-align: center; margin: auto; width: 60%;">'
                                        + 'The image for this page is missing from the image set.'
                                        + '</h4><p/></div>';
            facsimileViewer.classList.remove('loading');
            return console.log('FacsimileViewer:', 'image missing from image set');
        }


        console.log('FacsimileViewer:', 'load', facsimileUrl)
        showViewer()
        removeFacsimile();
        facsimileViewer.classList.remove('loading')
        var viewer = OpenSeadragon({
            id: "facsimile",
            prefixUrl: "node_modules/openseadragon/build/openseadragon/images/",
            constrainDuringPan: true,
            visibilityRatio: 1,
            minZoomLevel: 1,
            defaultZoomLevel: 1,
            sequenceMode: false,
            showFullPageControl: false,
            showNavigator: true,
            crossOriginPolicy: "Anonymous",
            tileSources: [facsimileUrl + '/info.json']
        });

        viewer.addHandler('open-failed',function(event){
            console.log("failed to open ", facsimileUrl);
        });

        viewer.addHandler('open',function(event){
            console.log("open ", facsimileUrl);
        });

        viewer.screenshot({
            showOptions: true,
            loadingDiv:  screenshotLoading,
            prefixUrl:   "resources/images/openseadragon-screenshot/",
        });

        $('#facsimile').on('open-failed', function (e) {
            console.log("image error: ", e)
        });
    }

    function removeFacsimile() {
        if (!facsimileViewer || !facsimileViewer.hasChildNodes()) {
            return console.log('FacsimileViewer:', 'no facsimile to remove')
        }
        for (var child = facsimileViewer.firstChild; child !== null; child = child.nextSibling) {
            facsimileViewer.removeChild(child);
        }
        facsimileViewer.classList.add('loading')
    }

    function hideViewer() {
        if (!facsimileViewer) {
            return
        }
        facsimileViewer.classList.add('hidden')
    }

    function showViewer() {
        if (!facsimileViewer) {
            return
        }
        facsimileViewer.classList.remove('hidden')
    }

    // ANNOTATIONS //

    var annotationPanel = document.getElementById("annotation-panel")
    var annotationPanelContainer = document.getElementById("annotations");
    var annotationPanelToggle = document.querySelector('.swap.btn')

    if (annotationPanelToggle) {
        annotationPanelToggle.addEventListener('click', function (e) {
            e.preventDefault();
            var classAdded = annotationPanelContainer.classList.toggle('hidden');
            annotationPanelToggle.classList.toggle('on', !classAdded);
        });
    }

    // This is a custom event that will be fired by the close box on the panel.
    window.addEventListener('closeAnnotationPanel', function (e) {
        e.preventDefault();
        hideAnnotationPanelContainer();
    });

    function hideAnnotationPanelContainer () {
      // TODO resize/ reposition document container
      if (!annotationPanelContainer || !annotationPanelToggle) return;
      annotationPanelContainer.classList.add('hidden');
      annotationPanelToggle.classList.remove('on');
    }

    function showAnnotationPanelContainer () {
      // TODO resize/ reposition document container
      if (!annotationPanelContainer || !annotationPanelToggle) return;
      annotationPanelContainer.classList.remove('hidden');
      annotationPanelToggle.classList.add('on');
    }
    window.addEventListener('wordsSelected', showAnnotationPanelContainer, true);

    function setAnnotations(annotationData) {
        // remove loading attribute
        if (!annotationPanel) return;

        annotationPanel.$.list.loading = false;

        if (!annotationData) { return }

        // this is to ensure we have an array
        var annotations = [];
        // single entry
        if (typeof annotationData === 'string' && annotationData.length) {
            annotations = [annotationData]
        }
        // multiple entries
        if (typeof annotationData === 'object' && annotationData && annotationData.length) {
            annotations = annotationData
        }

        // create HTML from strings using jQuery
        var annotationElements = annotations.map(function (annotationHTML) {
            return $(annotationHTML).get(0)
        })

        annotationPanel.$.list.addAnnotations(annotationElements)
    }

    function annopanelDragStart(event) {
        var rect = event.target.getBoundingClientRect();
        event.dataTransfer.setData("text/plain", (rect.left - event.clientX) + ',' + (rect.top - event.clientY));
    }

    function contentDragOver(event) {
        event.preventDefault();
        return false;
    }

    function contentDrop(event) {
        var offset = event.dataTransfer.getData("text/plain").split(',');
        annotationPanelContainer.style.left = (event.clientX + parseInt(offset[0],10)) + 'px';
        annotationPanelContainer.style.top = (event.clientY + parseInt(offset[1],10)) + 'px';
        event.preventDefault();
        return false;
    }

    if (annotationPanelContainer) {
        annotationPanelContainer.addEventListener("dragstart", annopanelDragStart, true);
    }
    var contentDiv = document.getElementById("content");
    if (contentDiv) {
        contentDiv.addEventListener("dragover", contentDragOver, true);
        contentDiv.addEventListener("drop", contentDrop, true);
    }

    // CONTENT / DOCUMENT //

    function loadPage(params, direction, animate, replaceContent) {
        var animOut = direction == "nav-next" ? "fadeOutLeft" : (direction == "nav-prev" ? "fadeOutRight" : "fadeOut");
        var animIn = direction == "nav-next" ? "fadeInRight" : (direction == "nav-prev" ? "fadeInLeft" : "fadeIn");

        var container = $("#content-container");
        console.log("Loading %s", params);

        $('.tooltip').remove();

        if (annotationPanel
            && annotationPanel.reset
            && typeof annotationPanel.reset === "function") {
            // reset selection
            annotationPanel.reset();
            // clear annotations in list
            annotationPanel.$.list.empty();
            // set loading attribute
            annotationPanel.$.list.loading = true;
        }

        removeFacsimile();

        $.ajax({
            url: appRoot + "/modules/ajax.xql",
            dataType: "json",
            data: params,
            error: function (xhr, status) {
                setAnnotations()
                showContent(container, animIn, animOut);
            },
            success: function (data) {
                $('.tooltip').remove();

                if (data.error) {
                    setAnnotations()
                    showContent(container, animIn, animOut);
                    return;
                }
                if (replaceContent) {
                    $(".content").replaceWith(data.content);
                }
                setAnnotations(data.annotations)

                // fix timing issue in firefox and safari
                // to reliably set selected spelling
                setTimeout(setSpelling, 100);

                setNavigationButtonState(toPreviousPageButton, data.previous)
                setNavigationButtonState(toNextPageButton, data.next)

                if (data.switchView) {
                    $("#switch-view").attr("href", data.switchView);
                    if (data.switchView.indexOf('view=div') !== -1) {
                        $('#switch-view').text('Switch to division view');
                    }
                    else {
                        $('#switch-view').text('Switch to page view');
                    }
                }

                initContent();
                if (animate) {
                    showContent(container, animIn, animOut);
                }
            }
        });
        if (animate) {
            container.addClass("animated " + animOut)
                .one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", function () {
                    $("#image-container img").css("display", "none");
            });
        }
    }

    function initContent() {
        $(".content .note").popover({
            html: true,
            trigger: "hover"
        });

        loadFacsimile(getFacsimileUrl())
    }

    function showContent(container, animIn, animOut, id) {
        if (!id) {
            window.scrollTo(0, 0);
        }
        container.removeClass("animated " + animOut);
        $("#content-container").addClass("animated " + animIn).one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", function () {
            $(this).removeClass("animated " + animIn);
            if (id) {
                var target = document.getElementById(id.substring(1));
                target && target.scrollIntoView();
            }
        });
    }


    function isMobile() {
        try {
            document.createEvent("TouchEvent");
            return true;
        }
        catch (e) {
            return false;
        }
    }

    function eXide(ev) {
        // try to retrieve existing eXide window
        var exide = window.open("", "eXide");
        if (exide && !exide.closed) {
            var snip = $(this).data("exide-create");
            var path = $(this).data("exide-open");
            var line = $(this).data("exide-line");

            // check if eXide is really available or it's an empty page
            var app = exide.eXide;
            if (app) {
                // eXide is there
                if (snip) {
                    exide.eXide.app.newDocument(snip, "xquery");
                } else {
                    exide.eXide.app.findDocument(path, line);
                }
                exide.focus();
                setTimeout(function () {
                    if ($.browser.msie ||
                        (typeof exide.eXide.app.hasFocus == "function" && !exide.eXide.app.hasFocus())) {
                        alert("Opened code in existing eXide window.");
                    }
                }, 200);
            } else {
                window.eXide_onload = function () {
                    console.log("onloaed called");
                    if (snip) {
                        exide.eXide.app.newDocument(snip, "xquery");
                    } else {
                        exide.eXide.app.findDocument(path);
                    }
                };
                // empty page
                exide.location = this.href.substring(0, this.href.indexOf('?'));
            }
            return false;
        }
        return true;
    }

    resize();

    $(".page-nav,.toc-link").click(function (ev) {
        console.log("navigation!!!!!!!!!!", this);

        ev.preventDefault();
        if ($(this).attr('disabled') === 'disabled') return;

        var relPath = this.pathname.replace(/^.*\/([^\/]+)$/, "$1");
        var url = "doc=" + relPath + "&" + this.search.substring(1);
        if (historySupport) {
            history.pushState(null, null, this.href);
        }
        loadPage(url, this.className.split(" ")[0], 1, 1);
    });

    $(".toc .toc-link").click(function (ev) {
        ev.preventDefault();
        $(".toc").offcanvas('hide');
    });

    $("#zoom-in").click(function (ev) {
        ev.preventDefault();
        var size = getFontSize();
        $("#content-inner").css("font-size", (size + 1) + "px");
    });

    $("#zoom-out").click(function (ev) {
        ev.preventDefault();
        var size = getFontSize();
        $("#content-inner").css("font-size", (size - 1) + "px");
    });

    $("#toggle-view-reg").click(function (ev) {
        ev.preventDefault();
        if (showStandardSpellings) {
            $(this).text("Show standard spellings");
            showStandardSpellings = false;
            $(".download-link").each(function( index, element ) {
                var typeAttr = $(element).attr('data-template-type');
                var newAttr;
                switch (typeAttr) {
                    case 'plain': // HTML -- use in-app transform
                        newAttr = $(element).attr('href').replace(/[&\?]standardize=(true|false)$/, '') + '&standardize=false';
                        break;
                    case 'tei':  // XML includes both standard and original
                        newAttr = $(element).attr('href');
                        break;
                    default:     // substitution for external e-book downloads
                        newAttr = $(element).attr('href').replace(/_standard\./, '.');
                }
                $(element).attr('href', newAttr);
            });
        } else {
            $(this).text("Show original spellings");
            showStandardSpellings = true;
            $(".download-link").each(function( index, element ) {
                var typeAttr = $(element).attr('data-template-type');
                var newAttr;
                switch (typeAttr) {
                    case 'plain': // HTML -- use in-app transform
                        newAttr = $(element).attr('href').replace(/[&\?]standardize=(true|false)$/, '') + '&standardize=true';
                        break;
                    case 'tei':  // XML includes both standard and original
                        newAttr = $(element).attr('href');
                        break;
                    default:     // substitution for external e-book downloads
                        newAttr = $(element).attr('href').replace(/\./, '_standard.');
                }
                $(element).attr('href', newAttr);
            });
        }
        setSpelling();
    });

    $(window).on("popstate", function (ev) {
        var url = "doc=" + window.location.pathname.replace(/^.*\/([^\/]+)$/, "$1") + "&" + window.location.search.substring(1) +
            "&id=" + window.location.hash.substring(1);
        console.log("popstate: %s", url);
        loadPage(url, "", 1, 1);
    }).on("resize", resize);

    $("#collapse-sidebar").click(function (ev) {
        ev.preventDefault();
        $("#sidebar").toggleClass("hidden");
        if ($("#sidebar").is(":visible")) {
            $("#right-panel").removeClass("col-md-12").addClass("col-md-9 col-md-offset-3");
        } else {
            $("#right-panel").addClass("col-md-12").removeClass("col-md-9 col-md-offset-3");
        }
        resize();
    });

    if (isMobile()) {
        $("#content-container").swipe({
            swipe: function (event, direction, distance, duration, fingerCount, fingerData) {
                var nav;
                if (direction === "left") {
                    nav = $(".nav-next").get(0);
                } else if (direction === "right") {
                    nav = $(".nav-prev").get(0);
                } else {
                    return;
                }
                var url = "doc=" + nav.pathname.replace(/^.*\/([^\/]+)$/, "$1") + "&" + nav.search.substring(1);
                if (historySupport) {
                    history.pushState(null, null, nav.href);
                }
                loadPage(url, nav.className.split(" ")[0], 1, 1);
            },
            allowPageScroll: "vertical"
        });
    }

    $("#recompile").click(function (ev) {
        ev.preventDefault();
        $("#messageDialog .message").html("Processing ...");
        $("#messageDialog").modal("show");
        $.ajax({
            url: appRoot + "/modules/regenerate.xql",
            dataType: "html",
            success: function (data) {
                $("#messageDialog .message").html(data).find(".eXide-open").click(eXide);
            },
            error: function (xhr, status) {
                $("#messageDialog .message").html(xhr.responseXML);
            }
        });
    });

    $("#reindex").click(function (ev) {
        ev.preventDefault();
        $("#messageDialog .message").html("Updating indexes ...");
        $("#messageDialog").modal("show");
        $.ajax({
            url: appRoot + "/modules/index.xql",
            dataType: "html",
            success: function (data) {
                $("#messageDialog .message").html(data);
            },
            error: function (xhr, status) {
                $("#messageDialog .message").html(xhr.responseXML);
            }
        });
    });

    $('.typeahead-meta').typeahead({
        items: 20,
        minLength: 4,
        source: function (query, callback) {
            var type = this.$element.attr('id');
            $.getJSON("../modules/autocomplete.xql?q=" + query + "&type=" + type, function (data) {
                callback(data || []);
            });
        },
        updater: function (item) {
            if (/[\s,]/.test(item)) {
                return '"' + item + '"';
            }
            return item;
        }
    });

    $('.typeahead-search').typeahead({
        items: 30,
        minLength: 4,
        source: function (query, callback) {
            var type = $("select[name='tei-target']").val();
            $.getJSON("../modules/autocomplete.xql?q=" + query + "&type=" + type, function (data) {
                callback(data || []);
            });
        }
    });

    $(".download-link").click(function (ev) {
        $("#pdf-info").modal("show");
        var token = $(this).attr("data-token");
        downloadCheck = window.setInterval(function () {
            var cookieValue = $.macaroon("simple.token");
            if (cookieValue == token) {
                window.clearInterval(downloadCheck);
                $.macaroon("simple.token", null);
                $("#pdf-info").modal("hide");
            }
        }, 250);
    });

    $(".eXide-open").click(eXide);

    window.addEventListener('WebComponentsReady', function () {
        // imports are loaded and elements have been registered
        //hide annotation panel
        hideAnnotationPanelContainer()

        var highToggle = document.getElementById('toggle-view-highlights')
        if (!highToggle) { return } // nothing to toggle

        var docs = [].slice.call(document.querySelectorAll('annotatable-document'))

        var readModeLabel = "Enable annotations"
        var annotationModeLabel = "Disable annotations"
        var highlightState = true // highlights are on by default
        highToggle.addEventListener('click', function (event) {
            event.preventDefault()
            highlightState = !highlightState
            docs.forEach(function (doc) {
                doc.highlight = highlightState
            })
            if (highlightState) {
                $(this).text(annotationModeLabel)
                return
            }
            $(this).text(readModeLabel)
        })
    });

    function initTooltip() {
        $(".tooltip").remove();
        $('body').tooltip({
            selector: 'tei-w',
            delay: 500,
            html: true,
            container: 'body',
            title: function () {
                var lemma = this.getAttribute('lemma');
                var reg = this.getAttribute('reg');
                var pos = this.getAttribute('pos');
                return '<div>' +
                    '<span>id: </span><span>' + this.id + '</span><br/>' +
                    '<span>lemma: </span><span>' + (lemma || 'N/A') + '</span><br/>' +
                    '<span>reg: </span><span>' + (reg || this.original) + '</span><br/>' +
                    '<span style="text-align: left">pos: </span><span>' + (pos || 'N/A') + '</span></div>';
            }
        });
    }

    window.addEventListener('showtooltip', function (e) {
        var target = e.target;
        $(target).one('hide.bs.tooltip', function () {
            $(this).removeAttr('data-toggle');
        });
        $('#' + target.id).attr('data-toggle', 'tooltip');
    });

    function initPageSlider(doc) {
        var slider = document.getElementById('page_slider');
        if (!slider) {
            return;
        }
        var w = slider.clientWidth;
        var pages = [];
        var max = 0;
        $('#page_slider_tooltip').tooltip();

        $.ajax({
            url: appRoot + "/modules/ajax-pages.xql",
            dataType: "json",
            data: "doc=" + doc + "&",
            error: function (xhr, status) {
                console.log("No page slider for you: " + status)
            },
            success: function (data) {
                if (data.error) {
                    console.log('Ajax error: ' + data.error);
                    return;
                }
                pages = data['pages'].slice();
                max = pages.length;
                $('#page_slider').prop({ value: 1, min: 1, max: max });
            }
        });

        var isDragging = false;

        var moveTip = (function(e) {
            if (isDragging) {
                var posPerc = (slider.value / max) * 100;
                var pixPos = (posPerc / 100) * w;
                pixPos += slider.offsetLeft;
                var pbid = pages[slider.value - 1].pbid;
                $('#page_slider_tooltip').css('margin-left', pixPos +'px').attr('title', pbid).tooltip('fixTitle').tooltip('show');
            }
        });

        slider.onmousedown = (function(e){
            isDragging = true;
            slider.addEventListener('mousemove', moveTip, false);
            $('#page_slider_tooltip').tooltip('show');
        });

        slider.onmouseup = (function(e){
            isDragging = false;
            slider.removeEventListener('mousemove', moveTip);
            $('#page_slider_tooltip').tooltip("hide");
            var pbid = pages[slider.value - 1].pbid;
            var page = pbid.substring(pbid.indexOf("-", pbid) + 1);
            var url = "doc=" + doc + "&page=" + page;
            loadPage( url, "nav-next", 1, 1);
            if (historySupport) {
                var historyUrl = doc + "?page=" + page;
                history.pushState(null, null, historyUrl);
            }
        });
    }

    if (document.location.pathname.match(/[^\/]+$/)) {
        var doc = document.location.pathname.match(/[^\/]+$/)[0];
        var search = document.location.search ? document.location.search.replace(/^\?/, '') : '';
        loadPage("doc=" + doc + "&" + search, "nav-next", 0, !search.length);
        initPageSlider(doc);
    }

    // Utilities for form processing.

    function resetForm(form) {
        // clearing inputs
        var inputs = form.getElementsByTagName('input');
        for (var i = 0; i < inputs.length; i++) {
            switch (inputs[i].type) {
                case 'hidden':
                case 'search':
                case 'text':
                    inputs[i].value = '';
                    break;
                case 'radio':
                case 'checkbox':
                    inputs[i].checked = false;
            }
        }

        // clearing selects
        var selects = form.getElementsByTagName('select');
        for (var i = 0; i < selects.length; i++)
            selects[i].selectedIndex = 0;

        // clearing textarea
        var text = form.getElementsByTagName('textarea');
        for (var i = 0; i < text.length; i++)
            text[i].innerHTML= '';

        return false;
    }

    $(".filter_reset").click(function (ev) {
        form = ev.target.form;
        resetForm(form);
        form.submit();
    });

    // All of the following is to make the state of the "more filter options"
    // button stick after form submit.

    var lastCollapseState = localStorage.getItem('lastCollapseState');

    if (!lastCollapseState) {
        lastCollapseState = [];
        localStorage.setItem('lastCollapseState', JSON.stringify(lastCollapseState));
    }
    else {
        lastCollapseStateArray = JSON.parse(lastCollapseState);
        var arrayLength = lastCollapseStateArray.length;
        for (var i = 0; i < arrayLength; i++) {
            var panel = '#'+lastCollapseStateArray[i];
            $(panel).addClass('in');
            $(panel + '_span').attr('class', 'glyphicon glyphicon-chevron-down');
        }
    }

    $('#more_filters').on('shown.bs.collapse', function() {
        lastCollapseState = JSON.parse(localStorage.getItem('lastCollapseState'));
        if ($.inArray($(this).attr('id'), lastCollapseState) == -1) {
            lastCollapseState.push($(this).attr('id'));
        };
        localStorage.setItem('lastCollapseState', JSON.stringify(lastCollapseState));
        $('#more_filters_span').attr('class', 'glyphicon glyphicon-chevron-down');
    });

    $('#more_filters').on('hidden.bs.collapse', function() {
        lastCollapseState = JSON.parse(localStorage.getItem('lastCollapseState'));
        lastCollapseState.splice( $.inArray($(this).attr('id'), lastCollapseState), 1 );
        localStorage.setItem('lastCollapseState', JSON.stringify(lastCollapseState));
        $('#more_filters_span').attr('class', 'glyphicon glyphicon-chevron-right');
    });

    // Handle form control focus changes.

    function _addFormGroupFocus(element) {
        var $element = $(element);
        $element.closest(".form-group").addClass("is-focused");
    }
    function _removeFormGroupFocus(element) {
        $(element).closest(".form-group").removeClass("is-focused"); // remove class from form-group
    }

    $(document).on("focus", ".form-control, .form-group.is-fileinput", function () {
        _addFormGroupFocus(this);
    })
    $(document).on("blur", ".form-control, .form-group.is-fileinput", function () {
        _removeFormGroupFocus(this);
    })
});
