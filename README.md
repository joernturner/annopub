# AnnoPub

AnnoPub is a TEI Publisher application based on the eXist XML database for
displaying and searching the texts in the EarlyPrint corpus and enabling
crowd-sourced corrections of those texts via its annotation features.
Reading, searching, and correcting are integrated as much as possible.  The
application could be adapted to other corpora with relative ease.

## Technical Notes

The main eXist application is called (for historical reasons arising from the
precursor project Shakespeare His Contemporaries) 'shc' and makes use of
another separate repository called 'annotation-client'. Annotation-client
contains the client-side code implementing the UI of AnnoPub's annotation
features. It is built upon [Polymer](http://polymer-project.org) which is an
implementation of W3C Web Components.

The shc application uses NodeJS and bower to incorporate the annotation-client
module and other JavaScript and CSS libraries (such as e.g. OpenSeaDragon for
the image display).

## Building shc

### Requirements

To build xar applications you need:

* Java >= 1.8
* Apache Ant
 
For Javascript:

* nodejs
* bower


### Building xar

In general eXistdb apps are built with Apache Ant which essentially compresses all needed components of the
the app into one archive file with the ending '.xar'.

To build the xar file just execute:

`
ant
`

### Application runtime dependencies

The shc application manages its runtime dependencies (JavaScript and CSS) via bower. The dependencies are listed in the
file 'bower.json'.

The annotation-client module is one of these dependencies. It lives in its own repository and is pulled into shc whenever a

`
bower update
`

is executed. When executing the ant build step is is automatically called under the hood.

bower manages its dependencies in a folder called 'bower_components'. This is not versioned in the repository as its contents are considered external libraries. The  .gitignor file therefore contains an exclude for that directory.



