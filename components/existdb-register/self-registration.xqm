xquery version "3.0";

module namespace registration="http://existsolutions.com/registration";
import module namespace registration-config="http://existsolutions.com/registration-config" at "self-registration-config.xqm";

(:~
    user self-registration

    This query can be called by a form that send a username and password for registering with an app.

    @param $user 'user' in the form of an email address
    @param $pass 'pass' to use for logging into the app
:)

declare function registration:register($user as xs:string, $pass as xs:string){
    (: create 'registration collection and document if necessary :)
    let $debug := util:log("INFO", $registration-config:REGISTRATION-PATH)

    let $foo := if (xmldb:collection-available($registration-config:ROOT-COL))
                then ()
                else xmldb:create-collection("/db/apps/", $registration-config:APP || "/data")


    let $foo := if (xmldb:collection-available($registration-config:REGISTRATION-PATH))
                then ()
                else xmldb:create-collection($registration-config:ROOT-COL, $registration-config:REGISTRATION-COL)

    let $registrations-doc := $registration-config:REGISTRATION-PATH || "/registrations.xml"
    let $registrations := <registration-requests/>
    let $foo := if (doc-available($registrations-doc))
                then ()
                else xmldb:store($registration-config:REGISTRATION-PATH, "registrations.xml", $registrations)

    let $token := util:uuid()
    let $expires := current-dateTime() + xs:dayTimeDuration('P1D')

    (: store new reqistration request :)
    let $registration-request := <registration-request user="{$user}" pass="{$pass}" token="{$token}" expires="{$expires}" />

    (: create a registration request if the user not already sent one :)
    let $foo := if (exists(collection($registration-config:REGISTRATION-PATH)//registration-request[@user eq $user]))
                then ()
                else
                    update insert $registration-request into collection($registration-config:REGISTRATION-PATH)//registration-requests

    return $token
};
