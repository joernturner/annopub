xquery version "3.1";

import module namespace registration="http://existsolutions.com/registration" at "self-registration.xqm";
import module namespace xqmail="http://existsolutions.com/mailmodule" at "../xqmail/xqmail.xqm";
import module namespace registration-config="http://existsolutions.com/registration-config" at "self-registration-config.xqm";

let $user := request:get-parameter('user','natwhilk')
let $pass := request:get-parameter('password', '12345678')

let $token := registration:register($user, $pass)

let $url := "https://" || request:get-server-name() || "/" || $registration-config:APP || "/components/existdb-register/process-confirmation.xql"

(: the email subject for the confirmation mail :)
let $subject := "Your registration to EarlyPrint"
let $bodytext := "Welcome to EarlyPrint. &#10;&#10; To activate your account please click the following link: &#10;&#10;" || $url || "?token=" || $token
let $html :=
    <div>
        <h1>Welcome to EarlyPrint</h1>
        <p>To activate your account please click the following link:</p>
        <a href="{$url}?token={$token}">Activate your account now</a>
    </div>

return xqmail:sendMail($user,(),(),$subject,$bodytext,$html, ())
