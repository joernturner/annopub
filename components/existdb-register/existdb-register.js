/**
An element providing a basic registration form.

Example:

    <existdb-register></existdb-register>

Example:

    <existdb-register>
      <h2>Hello existdb-register</h2>
    </existdb-register>

@demo demo.html
*/

import { PolymerElement, html } from '../../resources/node_modules/@polymer/polymer/polymer-element.js';
import '../../resources/node_modules/@polymer/iron-ajax/iron-ajax.js';
import '../../resources/node_modules/@polymer/iron-pages/iron-pages.js';

export class eXistdbRegister extends PolymerElement {

    static get template() {
      return html`
        <style>
            :host {
                display: block;
                box-sizing: border-box;
                padding: 0px 10px 10px;
                width: 100%;
                font-family: "IM Fell English", serif;
                position: relative;
            }

            .form-control, .btn {
                font-family: "IM Fell English", serif;
                width: 100%;
                height:50px;
            }

            .form-group label {
                top: 7px;
            }

            a {
                text-decoration: none;
                color: #0288D1;
            }

            a:hover {
                text-decoration: underline;
            }

            .row {
                width: 100%;
                float: none;
            }

            .left {
                position: absolute;
                left: 20px;
                bottom: 10px
            }

            .right {
                position: absolute;
                right: 20px;
                bottom: 10px;
            }

            .btn-group-justified {
                display: inline-block;
            }

            .alert.alert-error {
                background-color: white;
                border: 3px solid #ed6363;
                margin: 15px 15px 15px 15px;
                padding: 15px 15px 15px 15px;
            }

            .panel {
                margin-bottom: 20px;
                background-color: #fff;
                border-radius: 4px;
                box-shadow: 0 1px 1px rgba(0,0,0,.05);
            }
            .panel-body {
                padding: 15px;
            }
            .h4, h4 {
                font-size: 18px;0%;
                margin-left: 10px;
                margin-top: 10px;
                margin-bottom: 10px;
            }
            .form-group {
                margin-bottom: 15px;
            }
            .form-control {
                display: block;
                line-height: 1.42857143;
                color: #555;
                margin: 0;
                padding-bottom: 0;
                font-size: 14px;
            }

            .btn {
                display: inline-block;
                font-weight: 400;
                text-align: center;
                white-space: nowrap;
                vertical-align: middle;
                touch-action: manipulation;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857143;
                -webkit-user-select: none;
                -moz-user-select: none;
                user-select: none;
            }
            .btn,
            .input-group-btn .btn {
              border: none;
              border-radius: 2px;
              margin: 5px 1px;
              text-transform: uppercase;
              letter-spacing: 0;
              will-change: box-shadow, transform;
              transition: box-shadow 0.2s cubic-bezier(0.4, 0, 1, 1), background-color 0.2s cubic-bezier(0.4, 0, 0.2, 1), color 0.2s cubic-bezier(0.4, 0, 0.2, 1);
              outline: 0;
              cursor: pointer;
              text-decoration: none;
              background: 0 0;
            }
            .btn:not(.btn-raised),
            .btn:not(.btn-raised).btn-default,
            .input-group-btn .btn:not(.btn-raised),
            .input-group-btn .btn:not(.btn-raised).btn-default {
              color: #4f4f4f;
            }
            .panel-heading {
                padding: 10px 10px;
                border-bottom: 1px solid transparent;
                border-top-left-radius: 3px;
                border-top-right-radius: 3px;
            }
            .panel.panel-default > .panel-heading,
            .panel > .panel-heading {
              background-color: #4f4f4f;
              color: white;
              opacity: 1.0;
            }
            .btn.btn-raised,
            .btn.btn-raised.btn-primary {
              padding: 15px 0;
              background-color: #4f4f4f;
              color: white;
            }
            .btn.btn-raised:not(.btn-link):hover.btn-primary,
            .btn.btn-raised:not(.btn-link):hover.btn-primary {
              background-color: rgba(153, 153, 153, 0.2);
              color: #4f4f4f;
            }

            .close {
                float: right;
                font-size: 21px;
                font-weight: 700;
                line-height: 1;
                color: white;
                background-color: #4f4f4f;
                text-shadow: 0 1px 0 #fff;
                opacity: 1.0;
                border: 0;
                padding: 0;
            }
            .close:focus, .close:hover {
                color: #bfbfbf;
                text-decoration: none;
                cursor: pointer;
            }
            button {
                overflow: visible;
            }
            .hidden {
                display: none;
                visibility: hidden;
            }
            .text-right {
                float: right;
            }
            .form-group.is-focused .form-control {
                outline: 0;
                background-image: linear-gradient(#4f4f4f, #4f4f4f), linear-gradient(#d2d2d2, #d2d2d2);
                background-size: 100% 2px,100% 1px;
                box-shadow: none;
                transition-duration: .3s;
            }
            .form-control,
            .form-group .form-control {
                border: 0;
                background-image: linear-gradient(#4f4f4f, #4f4f4f), linear-gradient(#d2d2d2, #d2d2d2);
                background-size: 0 2px,100% 1px;
                background-repeat: no-repeat;
                background-position: center bottom, center calc(99%);
                background-color: rgba(0, 0, 0, 0);
                transition: background 0s ease-out;
                float: none;
                box-shadow: none;
                border-radius: 0;
            }
        </style>

        <iron-pages id="registerpages" selected="0">
            <div class="login">
                <div id="registrationerror" class="alert alert-error hidden">{{error}}</div>
                <form id="eXistdbRegister" action="{{target}}" method="post" on-submit="validate">
                    <div class="form-group label-floating is-empty">
                        <label class="control-label">Email</label>
                        <input type="email" id="user" required="required" name="user" class="form-control" autofocus="autofocus" autocomplete="username" on-focus="_addFormGroupFocus" on-blur="_removeFormGroupFocus"/>
                    </div>
                    <div class="form-group  label-floating is-empty">
                        <label class="control-label">Password</label>
                        <input type="password" id="pass" name="password" class="form-control" required="required" autocomplete="new-password" on-focus="_addFormGroupFocus" on-blur="_removeFormGroupFocus"/>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary btn-raised form-control">
                                Register
                            </button>
                        </div>
                    </div>
                </form>
            </div>

            <div>
                <h4>Your registration has been sent.</h4>
                <p>Please consult your inbox to confirm your registration.</p>
            </div>
        </iron-pages>
`;
    }

  static get properties() {
    return {
      response: String,
      target: String,
      error: String
    }
  }

  showConfirmation () {
      this.$.registerpages.selectIndex(1);
  }

  connectedCallback() {
    super.connectedCallback();
    this._reset();

    var registerForm = $('#eXistdbRegister', this.shadowRoot);
    var that = this;
    this.validator = registerForm.validate({
      rules: {
          user: {
            required: true,
            email: true
          },
          pass: {
            required: true,
            minlength: 6
          }
        },
      messages:{
          user: 'Please enter a valid email address as user name.',
          pass: {
            required: "Please provide a password",
            minlength: jQuery.validator.format("Password must be at least {0} characters long")
          }
      },
      submitHandler:function(form){
          that.submitForm();
      }
    });
  }

  disconnectedCallback() {
    this._reset();
  }

  validate(event) {
      console.log("validate");
      event.preventDefault();
      return this.validator.form();
  }

  submitForm() {
      var registerForm = $('#eXistdbRegister', this.shadowRoot);
      var fData = registerForm.serialize();
      var that = this;
      $.ajax({
          type: "POST",
          url: this.target,
          data: fData,
          dataType: 'xml',
          statusCode: {
              400: function(message) {
                  var msg = $(message.responseXML).find('message').text();
                  that.error = msg;
              },
              404: function() {
                  that.error = "page not found";
              }
          }

      }).done(function(data){
          that.showConfirmation();
      });

      return false;
  }

  _reset() {
    // registration form values
    this.$.registrationerror.classList.add("hidden");
    this.$.user.value="";
    this.$.pass.value="";
    this.$.registerpages.selectIndex(0);
  }

    // Handle form control focus changes.

  _addFormGroupFocus(event) {
    var formGroup = event.target.parentNode;
    formGroup.classList.add("is-focused");
  }

  _removeFormGroupFocus(event) {
    var formGroup = event.target.parentNode;
    formGroup.classList.remove("is-focused");
  }
}

customElements.define('existdb-register', eXistdbRegister);
