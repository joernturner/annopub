/**
An element providing a login dialog.  It targets the "login" resource in
the controller and processes the JSON response.

Example:

    <existdb-login></existdb-login>

Example:

    <existdb-login>
      <h2>Hello existdb-login</h2>
    </existdb-login>

@demo demo.html
*/
import { PolymerElement, html } from '../../resources/node_modules/@polymer/polymer/polymer-element.js';
import '../../resources/node_modules/@polymer/iron-pages/iron-pages.js';
import '../../resources/node_modules/@polymer/iron-form/iron-form.js';
import '../existdb-register/existdb-register.js';

export class eXistdbLogin extends PolymerElement {

    static get template() {
      return html`
        <style>
            :host {
                display: block;
                box-sizing: border-box;
                padding: 0px 20px 40px;
                width: 100%;
                font-family: "IM Fell English", serif;
                position: relative;
            }

            .form-control, .btn {
                font-family: "IM Fell English", serif;
                width: 100%;
                height:50px;
            }

            .form-group label {
                top: 7px;
            }

            a {
                text-decoration: none;
                color: #0288D1;
            }

            a:hover {
                text-decoration: underline;
            }

            .row {
                width: 100%;
                float: none;
            }

            .btn-group-justified {
                display: inline-block;
            }

            .alert.alert-error {
                background-color: white;
                border: 3px solid #ed6363;
                margin: 15px 15px 15px 15px;
                padding: 15px 15px 15px 15px;
            }

            .panel {
                margin-bottom: 80px;
                background-color: #fff;
                border-radius: 4px;
                box-shadow: 0 1px 1px rgba(0,0,0,.05);
            }
            .panel-body {
                padding: 15px;
            }
            .h4, h4 {
                font-size: 18px;0%;
                margin-left: 10px;
                margin-top: 10px;
                margin-bottom: 10px;
            }
            .form-group {
                margin-bottom: 15px;
            }
            .form-control {
                display: block;
                line-height: 1.42857143;
                color: #555;
                margin: 0;
                padding-bottom: 0;
                font-size: 14px;
            }

            .material-icons {
                font-family: 'Material Icons';
                font-weight: normal;
                font-style: normal;
                font-size: 24px;
                line-height: 1;
                letter-spacing: normal;
                text-transform: none;
                display: inline-block;
                white-space: nowrap;
                word-wrap: normal;
                direction: ltr;
                text-rendering: optimizeLegibility;
                -webkit-font-smoothing: antialiased;
            }

            .btn {
                display: inline-block;
                font-weight: 400;
                text-align: center;
                white-space: nowrap;
                vertical-align: middle;
                touch-action: manipulation;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857143;
                -webkit-user-select: none;
                -moz-user-select: none;
                user-select: none;
            }
            .btn,
            .input-group-btn .btn {
              border: none;
              border-radius: 2px;
              margin: 5px 1px;
              text-transform: uppercase;
              letter-spacing: 0;
              will-change: box-shadow, transform;
              transition: box-shadow 0.2s cubic-bezier(0.4, 0, 1, 1), background-color 0.2s cubic-bezier(0.4, 0, 0.2, 1), color 0.2s cubic-bezier(0.4, 0, 0.2, 1);
              outline: 0;
              cursor: pointer;
              text-decoration: none;
              background: 0 0;
            }
            .btn:not(.btn-raised),
            .btn:not(.btn-raised).btn-default,
            .input-group-btn .btn:not(.btn-raised),
            .input-group-btn .btn:not(.btn-raised).btn-default {
              color: #4f4f4f;
            }
            .panel-heading {
                padding: 10px 10px;
                border-bottom: 1px solid transparent;
                border-top-left-radius: 3px;
                border-top-right-radius: 3px;
            }
            .panel.panel-default > .panel-heading,
            .panel > .panel-heading {
              background-color: #4f4f4f;
              color: white;
              opacity: 1.0;
            }
            .btn.btn-raised,
            .btn.btn-raised.btn-primary {
              padding: 15px 0;
              background-color: #4f4f4f;
              color: white;
            }
            .btn.btn-raised:not(.btn-link):hover.btn-primary,
            .btn.btn-raised:not(.btn-link):hover.btn-primary {
              background-color: rgba(153, 153, 153, 0.2);
              color: #4f4f4f;
            }

            .close {
                float: right;
                font-size: 21px;
                font-weight: 700;
                line-height: 1;
                color: white;
                background-color: #4f4f4f;
                text-shadow: 0 1px 0 #fff;
                opacity: 1.0;
                border: 0;
                padding: 0;
            }
            .close:focus, .close:hover {
                color: #bfbfbf;
                text-decoration: none;
                cursor: pointer;
            }
            button {
                overflow: visible;
            }
            .hidden {
                display: none;
                visibility: hidden;
            }
            .text-right {
                float: right;
            }
            .btn-default:hover {
                color: #333;
                background-color: #e6e6e6;
                border-color: #adadad;
            }
            .form-group.is-focused .form-control {
                outline: 0;
                background-image: linear-gradient(#4f4f4f, #4f4f4f), linear-gradient(#d2d2d2, #d2d2d2);
                background-size: 100% 2px,100% 1px;
                box-shadow: none;
                transition-duration: .3s;
            }
            .form-control,
            .form-group .form-control {
                border: 0;
                background-image: linear-gradient(#4f4f4f, #4f4f4f), linear-gradient(#d2d2d2, #d2d2d2);
                background-size: 0 2px,100% 1px;
                background-repeat: no-repeat;
                background-position: center bottom, center calc(99%);
                background-color: rgba(0, 0, 0, 0);
                transition: background 0s ease-out;
                float: none;
                box-shadow: none;
                border-radius: 0;
            }
        </style>
        <iron-pages id="loginpages" selected="0">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" on-click="_close">
                        <i class="material-icons">close</i>
                    </button>
                    <h4>Login</h4>
                </div>
                <div class="panel-body">
                <iron-form id="loginform">
                <form action="{{ target }}" method="POST" allow-redirect="false" on-iron-form-presubmit="_presubmit">
                    <div class="form-group label-floating is-empty">
                        <label class="control-label">User ID</label>
                        <input id="username" type="text" name="user" class="form-control" autofocus="autofocus" required="true" autocomplete="username" on-focus="_addFormGroupFocus" on-blur="_removeFormGroupFocus"/>
                    </div>
                    <div class="form-group  label-floating is-empty">
                        <label class="control-label">Password</label>
                        <input id="password" type="password" name="password" class="form-control" required="true" autocomplete="current-password" on-focus="_addFormGroupFocus" on-blur="_removeFormGroupFocus"/>
                    </div>
                    <div id="loginerror" class="col-md-12 alert alert-error hidden">
                        <span id="loginmessage">
                    </span></div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <paper-button on-tap="_submit" class="btn btn-primary btn-raised form-control">Sign me in
                            </paper-button>
                        </div>
                    </div>
                </form>
                </iron-form>
                </div>
                <div class="btn-group btn-group-justified">
                   <div class="col-md-6">
                        <a href="#" class="btn btn-default" on-click="showRegistration">Register
                            <div class="material-icons">arrow_forward</div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" on-click="_close">
                        <i class="material-icons">close</i>
                    </button>
                    <h4>Register</h4>
                </div>
                <div class="panel-body">
                    <existdb-register target="/exist/apps/shc/components/existdb-register/shc-register.xql"></existdb-register>
                </div>
                <div class="btn-group btn-group-justified">
                    <div class="col-md-6">
                        <a on-click="showLogin" href="#" class="btn btn-default">
                            <div class="material-icons">arrow_back</div> back to login
                        </a>
                    </div>
                </div>
            </div>
        </iron-pages>
    `;
    }

    static get properties() {
        return {
            target: String,
            original_target: String
        }
    }

  // Element Lifecycle

    ready() {
        super.ready();
        this.addEventListener('iron-form-response', this._response);
        this.addEventListener('iron-form-error', this._error);
    }

    connectedCallback() {
        super.connectedCallback();
        this.original_target = window.location
        var protocol = window.location.protocol;
        var host = window.location.hostname;
        var port = window.location.port;
        if (protocol === 'http:' && port.length > 0 && port != '80'
            || protocol === 'https:' && port.length > 0 && port != '443') {
            host += ':' + port;
        }
        var origin = protocol + '//' + host;
        this.target = origin + '/exist/apps/shc/login';
        console.log('this.target: ' + this.target);
        //$.material.init();
        this._reset();
    }

    disconnectedCallback() {
        this._reset();
    }

    // Element Behavior
    showRegistration(){
        this._reset();
        this.$.loginpages.selectIndex(1);
        var registerComponent = this.shadowRoot.querySelector('existdb-register');
        if (registerComponent) {
            registerComponent._reset();
        }
    }

    showLogin(){
        this._reset();
        this.$.loginpages.selectIndex(0)
    }

    _presubmit(e) {
        this.$.loginform.setAttribute("action", this.target);
    }

    _submit() {
        this.$.loginform.submit();
    }

    _response(e) {
        var message = "Unknown error";
        if (typeof(e.detail.response) != "undefined" && "user" in e.detail.response) {
            console.log('login succeeded', e.detail.response);
            this.$.loginerror.classList.add("hidden");
            window.location.reload(false);
        }
        else {
            if ("fail" in e.detail.response) {
                message = e.detail.response.fail;
            }
            console.log('login failed', e.detail.response);
            this.$.loginerror.classList.remove("hidden");
            this.$.loginmessage.innerHTML = message;
        }
    }

    _error(e) {
        console.log("_error 1: ", e);
        var message = "Could not log in: no server response";
        if (typeof(e.detail) != "undefined" && "error" in e.detail && e.detail.error.length > 0) {
            message = e.detail.error;
            this.$.loginerror.classList.remove("hidden");
            this.$.loginmessage.innerHTML = message;
        }
        else {
        /* Ignore non-specific errors until iron-form can be replaced. It
            * downgrades to insecure content which gets blocked and throws an
            * error, but meanwhile the login succeeded.
            */
            window.location.reload(true)
            window.location.href = this.original_target
            window.location.reload(true);
        }
        console.log("_error 2: ", message);
    }

    _reset() {
        this.$.loginerror.classList.add("hidden");
        this.$.loginmessage.innerHTML = "";
        this.$.username.value="";
        this.$.password.value="";
    }

    _close(e) {
        this._reset();
        var dialog = document.getElementById("loginDialog");
        $(dialog).modal("toggle");
    }

    // Handle form control focus changes.

    _addFormGroupFocus(event) {
        var formGroup = event.target.parentNode;
        formGroup.classList.add("is-focused");
    }

    _removeFormGroupFocus(event) {
        var formGroup = event.target.parentNode;
        formGroup.classList.remove("is-focused");
    }
}

customElements.define('existdb-login', eXistdbLogin);
