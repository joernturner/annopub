xquery version "3.0";

module namespace xqmail-config = "http://existsolutions.com/mailmodule-config";

declare variable $xqmail-config:FROM := "shc-registration@earlyprint.org";
declare variable $xqmail-config:PASSWORD := "@@PASSWORD@@";
declare variable $xqmail-config:CC := "";
declare variable $xqmail-config:BCC := "";

declare variable $xqmail-config:SERVER-PROPERTIES :=
    <properties>
        <property name="mail.debug" value="true"/>
        <property name="mail.smtp.starttls.enable" value="true"/>
        <property name="mail.smtp.auth" value="true"/>
        <property name="mail.smtp.port" value="587"/>
        <property name="mail.smtp.host" value="mail.earlyprint.org"/>
        <property name="mail.smtp.user" value="{$xqmail-config:FROM}"/>
        <property name="mail.smtp.password"  value="{$xqmail-config:PASSWORD}"/>
    </properties>;
